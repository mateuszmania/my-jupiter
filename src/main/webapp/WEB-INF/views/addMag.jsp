<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>ADD MAGAZINE</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/style_add.css" rel="stylesheet">
</head>
<body>
	<div class = "black_background">
		<div class = "middle_background">
			<form method="post" action="/saveMag" accept-charset="UTF-8" style="color:white">
				<table>
					<tr>
						<th>ACRONYM:</th>
						<td><input type="text" name="acronym"/></td>
					</tr>
					<tr>
						<th>NAME:</th>
						<td><input type="text" name="name" /></td>
					</tr>	
					<tr>
						<th>ADDRESS:</th>
						<td><input type="text" name="address" /></td>
					</tr>
					<tr>
						<th>MAIL:</th>
						<td><input type="text" name="mail" /></td>
					</tr>
					<tr>
						<th>PHONE:</th>
						<td><input type="text" name="phone" /></td>
					</tr>						
				</table>
				<br/>
				<input type="submit" class="myButton" value="Add new magazine!">
			</form>
		</div>
		<div class="bottom_background">
			<a href="magazines" class="myButton" >Back!</a>
		</div>
	</div>
</body>
</html>