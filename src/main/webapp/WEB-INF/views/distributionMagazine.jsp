<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>STOCK</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style_add.css" rel="stylesheet">
</head>
<body>
	<div class="black_background">
		<div class="middle_background">
		<form method="post" action="/magazineTransfer" accept-charset="UTF-8">
		<table>
			<tr>
				<th>Target:</th>
				<td>
					<input name="target" value="WR" disabled="disabled"/>
				</td>
			</tr>
			<tr>
				<th>Product:</th>
				<td>
					<input list="products" name="transferProduct">
					<datalist id="products">
						<c:forEach var = "pro" items = "${lists2}">
					  		<option value="${pro.productCode}">
						</c:forEach>
					</datalist>
				</td>
			</tr>
			<tr>
				<th>Number:</th>
				<td><input type="number" name="number"/></td>
			<tr>
		</table>
		<br/>
		<input type="submit" class="myButton" value="Distribution!">
		</form>
			<div>
				<a href="../stock"
					class="myButton">Back!</a>
			</div>
		</div>
	</div>
</body>
</html>