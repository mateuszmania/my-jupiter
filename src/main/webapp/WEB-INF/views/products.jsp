<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
	<title>PRODUCTS</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="css/style_table.css" rel="stylesheet">
</head>
<body>
<div class = "black_background">
    <div class = "middle_background">
		<table>
		  <tr>
		    <th>ID</th>
		    <th>CODE</th>
		    <th>NAME</th>
		    <th>PRICE</th>
		    <th>WEIGHT</th>
		    <th>VOLUME</th>
		    <th>CRUD</th>
		  </tr>
		   <c:forEach var = "list" items = "${lists}">
		  <tr>
		    <td>${list.productId}</td>
		    <td>${list.productCode}</td>
		    <td>${list.productName}</td>
		    <td>${list.productPrice}</td>
		    <td>${list.productWeight}</td>
		    <td>${list.productVolume}</td>
		    <td>
		        <a href="/view/${list.productId}">View</a>
		        <a href="/delete/${list.productId}">Delete</a>
		        <a href="/edit/${list.productId}">Edit</a>
		    </td>
		  </tr>
		  </c:forEach>
		</table>
	</div>
	<div class="bottom_background">
		<a href="add" class="myButton" style="float:left">New product!</a>
		<a href="/" class="myButton" style="float:right">Back!</a>
	</div>
</div>
</body>
</html>