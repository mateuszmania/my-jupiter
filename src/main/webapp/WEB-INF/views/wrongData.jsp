<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>STOCK</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style_wrong.css" rel="stylesheet">
</head>
<body>
	<div class = "black_background">
		<div class = "top_background">
			<div class = "information">
				Operation canceled!<br/>
				Please check your data and try again...<br/>
<%--				${exception} --%>
			</div>
			<div class = "button_div">
				<a href="javascript:history.back()" class = "myButton">Back!</a>
			</div>
		</div>
	</div>
</body>
</html>