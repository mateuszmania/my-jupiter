<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>STOCK</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style_add.css" rel="stylesheet">
</head>
<body>
	<div class="black_background">
		<div class="middle_background">
		<form method="post" action="/transfer" accept-charset="UTF-8">
		<table>
			<tr>
				<th>Source:</th>
				<td>
					<input list="magazines" name="source">
					<datalist id="magazines">
						<c:forEach var = "list" items = "${lists}">
					  		<option value="${list.acronym}">
						</c:forEach>
					</datalist>
				</td>
			</tr>
			<tr>
				<th>Target:</th>
				<td>
					<input list="magazines" name="target">
					<datalist id="magazines">
						<c:forEach var = "list" items = "${lists}">
					  		<option value="${list.acronym}">
						</c:forEach>
					</datalist>
				</td>
			</tr>
			<tr>
				<th>Product:</th>
				<td>
					<input list="products" name="transferProduct">
					<datalist id="products">
						<c:forEach var = "pro" items = "${lists2}">
					  		<option value="${pro.productCode}">
						</c:forEach>
					</datalist>
				</td>
			</tr>
			<tr>
				<th>Number:</th>
				<td><input type="number" name="number" style = "clear: none"/></td>
			<tr>
		</table>
		<br/>
		<input type="submit" class="myButton" value="Distribution!">
		</form>
			<div>
				<a href="../stock" class = "myButton">Back!</a>
			</div>
		</div>
	</div>
</body>
</html>