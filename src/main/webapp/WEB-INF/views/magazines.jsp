<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
	<title>MAGAZINES</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="css/style_table.css" rel="stylesheet">
</head>
<body>
<div class = "black_background">
    <div class = "middle_background">
		<table>
		  <tr>
		    <th>ID</th>
		    <th>ACRONYM</th>
		    <th>NAME</th>
		    <th>ADDRESS</th>
		    <th>MAIL</th>
		    <th>PHONE</th>
		    <th>CRUD</th>
		  </tr>
		 <c:forEach var = "list" items = "${lists}">
		  <tr>
		    <td>${list.magazineId}</td>
		    <td>${list.acronym}</td>
		    <td>${list.name}</td>
		    <td>${list.address}</td>
		    <td>${list.mail}</td>
		    <td>${list.phone}</td>
		    <td>
		        <a href="/viewMag/${list.magazineId}">View</a>
		        <a href="/deleteMag/${list.magazineId}">Delete</a>
		        <a href="/editMag/${list.magazineId}">Edit</a>
		    </td>
		  </tr>
		  </c:forEach>
		</table>
	</div>
	<div class = "bottom_background">
		<a href="addMag" class = "myButton">NEW MAGAZINE</a>
		<a href="/" class = "myButton" style = "float: right">Back!</a>
	</div>
</div>
</body>
</html>