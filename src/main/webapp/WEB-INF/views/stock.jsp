<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
	<title>STOCK</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="css/style_table.css" rel="stylesheet">
</head>
<body>
<div class = "black_background">
	<div class = "top_background">
		<form method="POST" action="doSearch">
			ACRONYM: <input type="text" name="searchAcronym" value="${searchAcronym}" style="width:50px"/>
			CODE: <input type="text" name="searchCode" value="${searchCode}" style="width:250px;"/>
			<input type="submit" class = "myButton" value="Search"/>
		</form>
	</div>
    <div class = "middle_background" style = "top: 9%; height: 75%">
		<table>
		  <tr>
		    <th>ACRONYM</th>
		    <th>MAGAZINE NAME</th>
		    <th>PRODUCT CODE</th>
		    <th>PRODUCT NAME</th>
		    <th>QUANTITY</th>
		  </tr>
		   	<c:forEach var = "list" items = "${lists}">
		  <tr>
		  	<td>${list.magazine.acronym}</td>
		    <td>${list.magazine.name}</td>
		    <td>${list.product.productCode}</td>
		    <td>${list.product.productName}</td>
		    <td>${list.quantity}</td>
		  </tr>
		 	 </c:forEach>
		</table>
	</div>
	<div class = "bottom_background">
	 	<a href="distribution" class = "myButton">DISTRIBUTION</a>
		<a href="distributionMagazine" class = "myButton">MAGAZINE DISTRIBUTION</a>
		<a href="/" class = "myButton" style = "float: right">Back!</a>
	</div>
</div>
</body>
</html>