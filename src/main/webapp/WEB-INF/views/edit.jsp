<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>EDIT</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="../css/style_add.css" rel="stylesheet">
</head>
<body>
	<div class = "black_background">
		<div class = "middle_background">
			<form method="post" action="/saveAfterEdition" accept-charset="UTF-8">
				<table>
					<tr>
						<th>ID:</th>
						<td><input type="text" name="productId" value="${lists.productId}"></td>
					</tr>
					<tr>
						<th>Code:</th>
						<td><input type="text" name="code" value="${lists.productCode}"/></td>
					</tr>
					<tr>
						<th>Name:</th>
						<td><input type="text" name="name" value="${lists.productName}"/></td>
					</tr>	
					<tr>
						<th>Price:</th>
						<td><input type="text" name="price" value="${lists.productPrice}"/></td>
					</tr>
					<tr>
						<th>Weight:</th>
						<td><input type="text" name="weight" value="${lists.productWeight}"/></td>
					</tr>
					<tr>
						<th>Volume:</th>
						<td><input type="text" name="volume" value="${lists.productVolume}"/></td>
					</tr>						
				</table>
				<br/>
				<input type="submit" class="myButton" value="Submit">
			</form>
		</div>
		<div class = "bottom_background">
			<a href="../products" class = "myButton">Back!</a>
		</div>
	</div>
</body>
</html>