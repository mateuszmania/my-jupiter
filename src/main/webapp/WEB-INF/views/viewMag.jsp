<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>ADD</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/style_add.css" rel="stylesheet">
</head>
<body>
	<div class = "black_background">
		<div class = "middle_background">
			<table>
			  <tr>
			    <th>ACRONYM:</th>
			    <td>${lists.acronym}</td>
			  </tr>
			  <tr>
			     <th>NAME:</th>
			     <td>${lists.name}</td>
			   </tr>
			   <tr>
			       <th>ADDRESS:</th>
			       <td>${lists.address}</td>
			   </tr>
			   <tr>
			       <th>MAIL:</th>
			       <td>${lists.mail}</td>
			   </tr>
			   <tr>
			       <th>PHONE:</th>
			       <td>${lists.phone}</td>
			   </tr>
			</table>
		</div>
		<div class="bottom_background">
			<a href="../magazines" class="myButton" >Back!</a>
		</div>
	</div>
</body>
</html>