<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>

<html>
<head>
<title>JUPITER</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="../css/style.css" rel="stylesheet">
</head>
<body>
	<div class="black_background">
		<div class="middle_background">
			<div style="height: 250px; width: 100%">
				<div class="header">
					JUPITER
				</div>
			</div>
			<div style="height: 350px; width: 100%">
				<div class="header2">
					<div><a href="products" class="button">PRODUCTS</a></div><br>
					<div><a href="magazines" class="button">MAGAZINES</a></div> <br>
					<div><a href="stock" class="button">DISTRIBUTION</a></div><br>
				</div>
			</div>	
		</div>
		<div class="bottom_background">
			<div class="header3">
				<a>Autor: Mateusz Mania</a>
			</div>
		</div>
	</div>
</body>
</html>