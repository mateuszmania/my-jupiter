<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>ADD</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/style_add.css" rel="stylesheet">
</head>
<body>
	<div class="black_background">
		<div class="middle_background">
			<form method="post" action="/saveMagAfterEdition" accept-charset="UTF-8">
				<table>
					<tr>
						<th>ID:</th>
						<td><input type="text" name="magazineId" value="${lists.magazineId}"/></td>
					</tr>
					<tr>
						<th>ACRONYM:</th>
						<td><input type="text" name="acronym" value="${lists.acronym}"/></td>
					</tr>
					<tr>
						<th>NAME:</th>
						<td><input type="text" name="name" value="${lists.name}"/> </td>
					</tr>	
					<tr>
						<th>ADDRESS:</th>
						<td><input type="text" name="address" value="${lists.address}"/></td>
					</tr>
					<tr>
						<th>MAIL:</th>
						<td><input type="text" name="mail" value="${lists.mail}"/></td>
					</tr>
					<tr>
						<th>PHONE:</th>
						<td><input type="text" name="phone" value="${lists.phone}"/></td>
					</tr>						
				</table>
				<br/>
				<input type="submit" class="myButton" value="Submit">
			</form>
		</div>
		<div class="bottom_background">
			<a href="../magazines" class="myButton" >Back!</a>
		</div>
	</div>
</body>
</html>