<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>ADD</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="../css/style_add.css" rel="stylesheet">
</head>
<body>
	<div class = "black_background">
		<div class = "middle_background">
			<table>
				<tr>
					<th>ID:</th>
					<td>${lists.productId}</td>
				</tr>
				<tr>
					<th>CODE:</th>
					<td>${lists.productCode}</td>
				</tr>
				<tr>
					<th>NAME:</th>
					<td>${lists.productName}</td>
				</tr>
				<tr>
					<th>PRICE:</th>
					<td>${lists.productPrice}</td>
				</tr>
				<tr>
					<th>WEIGHT:</th>
					<td>${lists.productWeight}</td>
				</tr>
				<tr>
					<th>VOLUME:</th>
					<td>${lists.productVolume}</td>
				</tr>
			</table>
		</div>
		<div class = "bottom_background">
			<a href="../products" class = "myButton">Back!</a>
		</div>
	</div>
</body>
</html>