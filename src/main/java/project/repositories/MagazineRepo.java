package project.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import project.models.Magazine;

public interface MagazineRepo extends CrudRepository<Magazine, Long> {

	Magazine findByMagazineId(Integer magazineId);
	Magazine findByAcronym(String acronym);
	
	@Transactional
	@Modifying
	Long deleteByMagazineId(Integer magazineId);
}
