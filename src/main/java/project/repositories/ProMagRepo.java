package project.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import project.models.ProductMagazine;

public interface ProMagRepo extends CrudRepository<ProductMagazine, Long> {

	@Query(value = "SELECT s FROM ProductMagazine s JOIN s.pk.product p JOIN s.pk.magazine m WHERE m.acronym = :acronym")
	List<ProductMagazine> findByAcronym(@Param("acronym") String acronym);

	@Query(value = "SELECT s FROM ProductMagazine s JOIN s.pk.product p JOIN s.pk.magazine m WHERE p.productCode = :productCode")
	List<ProductMagazine> findByProductCode(@Param("productCode") String productCode);

	@Query(value = "SELECT s FROM ProductMagazine s JOIN s.pk.product p JOIN s.pk.magazine m WHERE p.productCode = :productCode AND m.acronym = :acronym")
	List<ProductMagazine> findByAcronymAndCode(@Param("acronym") String acronym,
			@Param("productCode") String productCode);

	@Query(value = "SELECT s FROM ProductMagazine s JOIN s.pk.product p JOIN s.pk.magazine m WHERE p.productCode = :productCode AND m.acronym = :acronym")
	ProductMagazine findByMagazine_AcronymAndProduct_productCode(@Param("acronym") String acronym,
			@Param("productCode") String productCode);
}
