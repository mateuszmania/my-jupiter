package project.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import project.models.Product;

public interface ProductRepo extends CrudRepository<Product, Long> {

	Product findByProductId(Integer productId);

	Product findByProductCode(String productCode);

	@Transactional
	@Modifying
	Long deleteByProductId(Integer productId);
}
