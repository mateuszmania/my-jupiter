package project.contoller;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import project.repositories.ProductRepo;
import project.services.ProductService;

@Controller
public class ProductController {

	@Autowired
	ProductRepo proRepo;
	
	@Autowired
	ProductService proService;

	@RequestMapping("/")
	public ModelAndView doStart() {
		ModelAndView mv = new ModelAndView("start");
		return mv;
	}

	@RequestMapping("/add")
	public ModelAndView doAdd() {
		ModelAndView mv = new ModelAndView("add");
		return mv;
	}

	@RequestMapping("/products")
	public ModelAndView doProducts() {
		ModelAndView mv = new ModelAndView("products");
		mv.addObject("lists", proRepo.findAll());
		return mv;
	}

	@RequestMapping(value = "/view/{productId}", method = RequestMethod.GET)
	public ModelAndView doView(@PathVariable("productId") Integer productId) {
		ModelAndView mv = new ModelAndView("view");
		mv.addObject("lists", proRepo.findByProductId(productId));
		return mv;
	}

	@RequestMapping(value = "/delete/{productId}", method = RequestMethod.GET)
	public ModelAndView doDelete(@PathVariable("productId") Integer productId) {
		ModelAndView mv = new ModelAndView("redirect:/products");
		proRepo.deleteByProductId(productId);
		return mv;
	}

	@RequestMapping(value = "/edit/{productId}", method = RequestMethod.GET)
	public ModelAndView doEdit(@PathVariable("productId") Integer productId) {
		ModelAndView mv = new ModelAndView("edit");
		mv.addObject("lists", proRepo.findByProductId(productId));
		return mv;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView doSave(@RequestParam("code") String code, @RequestParam("name") String name,
			@RequestParam("price") BigDecimal price, @RequestParam("weight") BigDecimal weight,
			@RequestParam("volume") BigDecimal volume) {
		ModelAndView mv = new ModelAndView("redirect:/products");
		proService.createProduct(code, name, price, weight, volume);
		return mv;
	}

	@RequestMapping(value = "/saveAfterEdition", method = RequestMethod.POST)
	public ModelAndView doSaveAfterEdition(@RequestParam("code") String code, @RequestParam("name") String name,
			@RequestParam("price") BigDecimal price, @RequestParam("weight") BigDecimal weight,
			@RequestParam("volume") BigDecimal volume, @RequestParam("productId") Integer productId) {
		ModelAndView mv = new ModelAndView("redirect:/products");
		proService.editProduct(code, name, price, weight, volume, productId);
		return mv;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest req, Exception ex) {
		ModelAndView mv = new ModelAndView("wrongData");
		mv.addObject("exception", ex.toString());
		return mv;
	}
}
