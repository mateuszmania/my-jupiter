package project.contoller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;

import project.repositories.MagazineRepo;
import project.repositories.ProMagRepo;
import project.repositories.ProductRepo;
import project.services.ProMagService;

@Controller
public class ProMagController {

	@Autowired
	ProMagRepo stockRepo;

	@Autowired
	ProMagService stockService;

	@Autowired
	MagazineRepo magRepo;

	@Autowired
	ProductRepo proRepo;

	@ResponseBody
	@RequestMapping("/stock")
	public ModelAndView doStock() {
		ModelAndView mv = new ModelAndView("stock");
		mv.addObject("lists", stockRepo.findAll());
		return mv;
	}

	@RequestMapping("/distribution")
	public ModelAndView doDistribution() {
		ModelAndView mv = new ModelAndView("distribution");
		mv.addObject("lists", magRepo.findAll());
		mv.addObject("lists2", proRepo.findAll());
		return mv;
	}

	@RequestMapping("/distributionMagazine")
	public ModelAndView doDistributionMagazine() {
		ModelAndView mv = new ModelAndView("distributionMagazine");
		mv.addObject("lists", magRepo.findAll());
		mv.addObject("lists2", proRepo.findAll());
		return mv;
	}

	@RequestMapping(value = "/transfer", method = RequestMethod.POST)
	public ModelAndView doTransfer(@RequestParam("transferProduct") String transferProduct,
			@RequestParam("source") String source, @RequestParam("target") String target,
			@RequestParam("number") Integer number) throws JsonProcessingException {
		ModelAndView mv = new ModelAndView("redirect:/stock");
		stockService.makeTransfer(transferProduct, source, target, number);
		return mv;
	}

	@RequestMapping(value = "/magazineTransfer", method = RequestMethod.POST)
	public ModelAndView doMagazineTransfer(HttpServletRequest request,
			@RequestParam("transferProduct") String transferProduct, @RequestParam("number") Integer number)
			throws NumberFormatException {
		ModelAndView mv = new ModelAndView("redirect:/stock");
		stockService.makeToMagazineTransfer(transferProduct, number);
		return mv;
	}

	@ResponseBody
	@RequestMapping(value = "doSearch", method = RequestMethod.POST)
	public ModelAndView doSearch(HttpServletRequest request, @RequestParam("searchCode") String searchCode,
			@RequestParam("searchAcronym") String searchAcronym) {
		ModelAndView mv = new ModelAndView("/stock");

		searchCode = request.getParameter("searchCode");
		searchAcronym = request.getParameter("searchAcronym");

		HttpSession session = request.getSession();
		session.setAttribute("searchCode", searchCode);
		session.setAttribute("searchAcronym", searchAcronym);

		if (!searchCode.isEmpty() && !searchAcronym.isEmpty()) {
			mv.addObject("lists", stockRepo.findByAcronymAndCode(searchAcronym, searchCode));
		} else if (!searchCode.isEmpty() && searchAcronym.isEmpty()) {
			mv.addObject("lists", stockRepo.findByProductCode(searchCode));
		} else if (searchCode.isEmpty() && !searchAcronym.isEmpty()) {
			mv.addObject("lists", stockRepo.findByAcronym(searchAcronym));
		} else if (searchCode.isEmpty() && searchAcronym.isEmpty()) {
			mv.addObject("lists", stockRepo.findAll());
		}
		return mv;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest req, Exception ex) {
		ModelAndView mv = new ModelAndView("wrongData");
		return mv;
	}
}
