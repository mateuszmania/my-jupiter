package project.contoller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import project.repositories.MagazineRepo;
import project.services.MagazineService;

@Controller
public class MagazineController {

	@Autowired
	MagazineRepo magRepo;
	
	@Autowired
	MagazineService magService;

	@RequestMapping("/addMag")
	public ModelAndView doAdd() {
		ModelAndView mv = new ModelAndView("addMag");
		return mv;
	}

	@RequestMapping("/magazines")
	public ModelAndView doMagazines() {
		ModelAndView mv = new ModelAndView("magazines");
		if (magRepo.count() == 0) {
			magService.createMagazine("Wrocław", "Borowska 116, 98-600 Wrocław", "WR", "669-423-878", "wroclaw1@jupiter.com");
			mv.addObject("lists", magRepo.findAll());
		} else {
			mv.addObject("lists", magRepo.findAll());
		}
		return mv;
	}

	@RequestMapping(value = "/viewMag/{magazineId}", method = RequestMethod.GET)
	public ModelAndView doView(@PathVariable("magazineId") Integer magazineId) {
		ModelAndView mv = new ModelAndView("viewMag");
		mv.addObject("lists", magRepo.findByMagazineId(magazineId));
		return mv;
	}

	@RequestMapping(value = "/deleteMag/{magazineId}", method = RequestMethod.GET)
	public ModelAndView doDelete(@PathVariable("magazineId") Integer magazineId) {
		ModelAndView mv = new ModelAndView("redirect:/magazines");
		magRepo.deleteByMagazineId(magazineId);
		return mv;
	}

	@RequestMapping(value = "/editMag/{magazineId}", method = RequestMethod.GET)
	public ModelAndView doEdit(@PathVariable("magazineId") Integer magazineId) {
		ModelAndView mv = new ModelAndView("editMag");
		mv.addObject("lists", magRepo.findByMagazineId(magazineId));
		return mv;
	}

	@RequestMapping(value = "/saveMag", method = RequestMethod.POST)
	public ModelAndView doSave(@RequestParam("name") String name, @RequestParam("address") String address,
			@RequestParam("acronym") String acronym, @RequestParam("phone") String phone,
			@RequestParam("mail") String mail) {
		ModelAndView mv = new ModelAndView("redirect:/magazines");
		magService.createMagazine(name, address, acronym, phone, mail);
		return mv;
	}

	@RequestMapping(value = "/saveMagAfterEdition", method = RequestMethod.POST)
	public ModelAndView doSaveAfterEdition(@RequestParam("name") String name, @RequestParam("acronym") String acronym,
			@RequestParam("address") String address, @RequestParam("phone") String phone,
			@RequestParam("mail") String mail, @RequestParam("magazineId") Integer magazineId) {
		ModelAndView mv = new ModelAndView("redirect:/magazines");
		magService.editMagazine(name, acronym, address, phone, mail, magazineId);
		return mv;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest req, Exception ex) {
		ModelAndView mv = new ModelAndView("wrongData");
		return mv;
	}
}
