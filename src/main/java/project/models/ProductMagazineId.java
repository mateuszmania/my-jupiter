package project.models;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class ProductMagazineId implements java.io.Serializable {

	private static final long serialVersionUID = -8705211207958527210L;
	private Product product;
    private Magazine magazine;

	@ManyToOne
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@ManyToOne
	public Magazine getMagazine() {
		return magazine;
	}

	public void setMagazine(Magazine magazine) {
		this.magazine = magazine;
	}

	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductMagazineId that = (ProductMagazineId) o;

        if (product != null ? !product.equals(that.product) : that.product != null) return false;
        if (magazine != null ? !magazine.equals(that.magazine) : that.magazine != null)
            return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (product != null ? product.hashCode() : 0);
        result = 31 * result + (magazine != null ? magazine.hashCode() : 0);
        return result;
    }
    
}