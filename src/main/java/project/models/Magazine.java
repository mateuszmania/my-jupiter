package project.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity(name = "magazine")
@Table(name = "magazine", uniqueConstraints = { @UniqueConstraint(columnNames = "ACRONYM") })
public class Magazine implements java.io.Serializable {

	private static final long serialVersionUID = -2942949270434227133L;
	private Integer magazineId;
	private String acronym;
	private String name;
	private String address;
	private String phone;
	private String mail;
	private Set<ProductMagazine> productMagazines = new HashSet<ProductMagazine>(0);

	public Magazine() {
	}

	public Magazine(String name, String acronym, String desc, String phone, String mail) {
		this.name = name;
		this.acronym = acronym;
		this.address = desc;
		this.phone = phone;
		this.mail = mail;
	}

	public Magazine(String name, String desc, String acronym, String phone, String mail,
			Set<ProductMagazine> productMagazines) {
		this.name = name;
		this.acronym = acronym;
		this.address = desc;
		this.phone = phone;
		this.mail = mail;
		this.productMagazines = productMagazines;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MAGAZINE_ID", unique = true, nullable = false)
	public Integer getMagazineId() {
		return this.magazineId;
	}

	public void setMagazineId(Integer magazineId) {
		this.magazineId = magazineId;
	}

	@Column(name = "ACRONYM", nullable = false, length = 2)
	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	@Column(name = "NAME", nullable = false, length = 20)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "ADDRESS", nullable = false)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "PHONE", nullable = false, length = 11)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "MAIL", nullable = false, length = 50)
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.magazine")
	public Set<ProductMagazine> getProductMagazines() {
		return this.productMagazines;
	}

	public void setProductMagazines(Set<ProductMagazine> productMagazines) {
		this.productMagazines = productMagazines;
	}

}