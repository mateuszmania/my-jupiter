package project.models;

import javax.persistence.*;

@Entity
@Table(name = "product_magazine")
@AssociationOverrides({ @AssociationOverride(name = "pk.product", joinColumns = @JoinColumn(name = "PRODUCT_ID")),
		@AssociationOverride(name = "pk.magazine", joinColumns = @JoinColumn(name = "MAGAZINE_ID")) })
public class ProductMagazine implements java.io.Serializable {

	private static final long serialVersionUID = -6334706442948286206L;
	private ProductMagazineId pk = new ProductMagazineId();
	private Integer quantity;

	public ProductMagazine() {
	}

	@EmbeddedId
	public ProductMagazineId getPk() {
		return pk;
	}

	public void setPk(ProductMagazineId pk) {
		this.pk = pk;
	}

	@Transient
	public Product getProduct() {
		return getPk().getProduct();
	}

	public void setProduct(Product product) {
		getPk().setProduct(product);
	}

	@Transient
	public Magazine getMagazine() {
		return getPk().getMagazine();
	}

	public void setMagazine(Magazine magazine) {
		getPk().setMagazine(magazine);
	}

	@Column(name = "QUANTITY", nullable = false, length = 10)
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ProductMagazine that = (ProductMagazine) o;

		if (getPk() != null ? !getPk().equals(that.getPk()) : that.getPk() != null)
			return false;

		return true;
	}

	public int hashCode() {
		return (getPk() != null ? getPk().hashCode() : 0);
	}
}