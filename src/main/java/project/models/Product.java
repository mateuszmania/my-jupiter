package project.models;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity(name = "product")
@Table(name = "product", uniqueConstraints = { @UniqueConstraint(columnNames = "PRODUCT_NAME"),
		@UniqueConstraint(columnNames = "PRODUCT_CODE") })
public class Product implements java.io.Serializable {

	private static final long serialVersionUID = -6343334904944878910L;
	private Integer productId;
	private String productCode;
	private String productName;
	private BigDecimal productPrice = new BigDecimal("1999.99");
	private BigDecimal productWeight = new BigDecimal("19.99");
	private BigDecimal productVolume = new BigDecimal("0.01");
	private Set<ProductMagazine> productMagazines = new HashSet<ProductMagazine>(0);

	public Product() {
	}

	public Product(String productCode, String productName, BigDecimal productPrice) {
		this.productCode = productCode;
		this.productName = productName;
		this.productPrice = productPrice;
	}

	public Product(String productCode, String productName, Set<ProductMagazine> productMagazines) {
		this.productCode = productCode;
		this.productName = productName;
		this.productMagazines = productMagazines;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PRODUCT_ID", unique = true, nullable = false)
	public Integer getProductId() {
		return this.productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_CODE", unique = true, nullable = false, length = 20)
	public String getProductCode() {
		return this.productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@Column(name = "PRODUCT_NAME", unique = true, nullable = false, length = 50)
	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "PRODUCT_PRICE", nullable = false)
	public BigDecimal getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(BigDecimal price) {
		this.productPrice = price;
	}

	@Column(name = "PRODUCT_WEIGHT", nullable = false)
	public BigDecimal getProductWeight() {
		return productWeight;
	}

	public void setProductWeight(BigDecimal productWeight) {
		this.productWeight = productWeight;
	}

	@Column(name = "PRODUCT_VOLUME", nullable = false)
	public BigDecimal getProductVolume() {
		return productVolume;
	}

	public void setProductVolume(BigDecimal productVolume) {
		this.productVolume = productVolume;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.product", cascade = CascadeType.ALL)
	public Set<ProductMagazine> getProductCategories() {
		return this.productMagazines;
	}

	public void setProductCategories(Set<ProductMagazine> productMagazines) {
		this.productMagazines = productMagazines;
	}

}