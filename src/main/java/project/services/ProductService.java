package project.services;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.models.Product;
import project.repositories.ProductRepo;

@Service
public class ProductService {

	@Autowired
	ProductRepo proRepo;

	public void createProduct(String code, String name, BigDecimal price, BigDecimal weight, BigDecimal volume) {
		Product product = new Product();
		product.setProductCode(code);
		product.setProductName(name);
		product.setProductPrice(price);
		product.setProductWeight(weight);
		product.setProductVolume(volume);
		proRepo.save(product);
	}

	public void editProduct(String code, String name, BigDecimal price, BigDecimal weight, BigDecimal volume,
			Integer productId) {
		Product product = proRepo.findByProductId(productId);
		product.setProductCode(code);
		product.setProductName(name);
		product.setProductPrice(price);
		product.setProductWeight(weight);
		product.setProductVolume(volume);
		proRepo.save(product);
	}
}
