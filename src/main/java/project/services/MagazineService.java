package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.models.Magazine;
import project.repositories.MagazineRepo;

@Service
public class MagazineService {

	@Autowired
	MagazineRepo magRepo;

	public void createMagazine(String name, String address, String acronym, String phone, String mail) {
		Magazine magazine = new Magazine();
		magazine.setAcronym(acronym);
		magazine.setName(name);
		magazine.setAddress(address);
		magazine.setMail(mail);
		magazine.setPhone(phone);
		magRepo.save(magazine);
	}
	
	public void editMagazine(String name, String acronym, String address, String phone, String mail, Integer magazineId) {
		Magazine magazine = magRepo.findByMagazineId(magazineId);
		magazine.setName(name);
		magazine.setAcronym(acronym);
		magazine.setAddress(address);
		magazine.setMail(mail);
		magazine.setPhone(phone);
		magRepo.save(magazine);
	}
}
