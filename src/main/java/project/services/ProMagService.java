package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.models.Magazine;
import project.models.Product;
import project.models.ProductMagazine;
import project.repositories.MagazineRepo;
import project.repositories.ProMagRepo;
import project.repositories.ProductRepo;

@Service
public class ProMagService {

	@Autowired
	ProMagRepo stockRepo;

	@Autowired
	MagazineRepo magRepo;

	@Autowired
	ProductRepo proRepo;

	public void makeTransfer(String transferProduct, String source, String target, Integer number) {
		ProductMagazine sourceMagazine = stockRepo.findByMagazine_AcronymAndProduct_productCode(source,
				transferProduct);
		ProductMagazine targetMagazine = stockRepo.findByMagazine_AcronymAndProduct_productCode(target,
				transferProduct);
		if (targetMagazine == null) {
			targetMagazine = createProMag(magRepo.findByAcronym(target), proRepo.findByProductCode(transferProduct));
			updateQuantity(sourceMagazine, targetMagazine, number);
		} else {
			updateQuantity(sourceMagazine, targetMagazine, number);
		}
	}

	public void makeToMagazineTransfer(String transferProduct, Integer number) {
		ProductMagazine targetMagazine = stockRepo.findByMagazine_AcronymAndProduct_productCode("WR", transferProduct);
		if (targetMagazine == null) {
			targetMagazine = createProMag(magRepo.findByAcronym("WR"), proRepo.findByProductCode(transferProduct));
			updateMagazineQuantity(targetMagazine, number);
		} else {
			updateMagazineQuantity(targetMagazine, number);
		}
	}

	public void updateQuantity(ProductMagazine sourceMagazine, ProductMagazine targetMagazine, Integer number) {
		sourceMagazine.setQuantity(subQuantity(sourceMagazine, number));
		targetMagazine.setQuantity(addQuantity(targetMagazine, number));
		stockRepo.save(sourceMagazine);
		stockRepo.save(targetMagazine);
	}

	public void updateMagazineQuantity(ProductMagazine targetMagazine, Integer number) {
		targetMagazine.setQuantity(addQuantity(targetMagazine, number));
		stockRepo.save(targetMagazine);
	}

	public ProductMagazine createProMag(Magazine mag, Product pro) {
		ProductMagazine newProMag = new ProductMagazine();
		newProMag.setMagazine(mag);
		newProMag.setProduct(pro);
		newProMag.setQuantity(0);
		return newProMag;
	}

	public int addQuantity(ProductMagazine targetMagazine, Integer number) {
		if (number > 0) {
			int targetQuantity = targetMagazine.getQuantity() + number;
			return targetQuantity;
		} else {
			throw new IllegalArgumentException("Only Positive Numbers!");
		}
	}

	public int subQuantity(ProductMagazine sourceMagazine, Integer number) {
		if (number > 0 && number <= sourceMagazine.getQuantity()) {
			int sourceQuantity = sourceMagazine.getQuantity() - number;
			return sourceQuantity;
		} else {
			throw new IllegalArgumentException("Only Positive Numbers!");
		}
	}
}
